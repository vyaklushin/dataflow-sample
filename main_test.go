package main

import (
	"testing"
	"time"

	"github.com/apache/beam/sdks/v2/go/pkg/beam/testing/passert"
	"github.com/apache/beam/sdks/v2/go/pkg/beam/testing/ptest"
	tr "gitlab.com/vyaklushin/dataflow-sample/internal/transformation"
)

func TestMain(m *testing.M) {
	ptest.Main(m)
}

func TestPipeline(t *testing.T) {
	tests := []struct {
		lines            []string
		parsedResults    []tr.ApplicationLog
		processedResults []tr.BigQueryRecord
	}{
		{
			[]string{
				`{"time": "2000-01-01T16:00:00.346Z", "meta.artifact_size": 500, "meta.root_namespace": "gitlab-org", "meta.project": "gitlab-org/gitlab"}`,
				`{"time": "2001-02-02T16:26:01Z", "route": "invalid_workhorse_route", "written_bytes": 200, "uri": "workhorse_uri"}`,
				`{"time": "2001-03-02T16:26:01Z", "route": "^/.+\\.git/git-upload-pack\\z", "written_bytes": 300, "uri": "/namespace/sub/project.git/git-upload-pack"}`,
				`{"does_not": "match"}`,
				`not_a_json`,
			},
			[]tr.ApplicationLog{
				{
					Time: "2000-01-01T16:00:00.346Z",
				},
				{
					Time:           "2001-02-02T16:26:01Z",
					WorkhorseRoute: "invalid_workhorse_route",
					WorkhorseBytes: 200,
					WorkhorseUri:   "workhorse_uri",
				},
				{
					Time:           "2001-03-02T16:26:01Z",
					WorkhorseRoute: "^/.+\\.git/git-upload-pack\\z",
					WorkhorseBytes: 300,
					WorkhorseUri:   "/namespace/sub/project.git/git-upload-pack",
				},
			},
			[]tr.BigQueryRecord{
				{
					Type:      "git",
					Bytes:     300,
					Project:   "namespace/sub/project",
					Namespace: "namespace",
					CreatedAt: time.Date(2001, time.March, 2, 16, 26, 1, 0, time.UTC),
				},
			},
		},
	}

	for _, test := range tests {
		p, s, lines := ptest.CreateList(test.lines)

		parsed := ParseJson(s, lines)
		passert.EqualsList(s, parsed, test.parsedResults)

		processed := ProcessJson(s, parsed)
		passert.EqualsList(s, processed, test.processedResults)

		ptest.RunAndValidate(t, p)
	}
}
