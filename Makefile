include .env

build:
	GOOS=linux GOARCH=amd64 go build -o _build/${BINARY_NAME} .

test:
	go test .

debug:
	go run main.go --input ${INPUT} --output=${OUTPUT} --debug=true

process:
	go run main.go --input ${INPUT} --project=${PROJECT} --table=${BQ_TABLE}

clickhouse:
	go run main.go --input ${INPUT} --clickhouse-host=${CLICKHOUSE_HOST} --clickhouse-user=${CLICKHOUSE_USER} --clickhouse-password=${CLICKHOUSE_PASSWORD} --clickhouse=true

gcloud_build:
	cp Dockerfile _build/Dockerfile
	gcloud builds submit --tag ${TEMPLATE_IMAGE} _build/

release_template:
	gcloud dataflow flex-template build ${TEMPLATE_PATH} \
	 --image ${TEMPLATE_IMAGE} \
	 --sdk-language "GO" \
	 --metadata-file "metadata.json"

dataflow_run:
	gcloud dataflow flex-template run "logparser-`date +%Y%m%d-%H%M%S`" \
	--template-file-gcs-location ${TEMPLATE_PATH} \
	--parameters input="${INPUT}" \
	--parameters table="${BQ_TABLE}" \
	--region "${REGION}"

dataflow_clickhouse_run:
	gcloud dataflow flex-template run "logparser-`date +%Y%m%d-%H%M%S`" \
	--template-file-gcs-location ${TEMPLATE_PATH} \
	--parameters input="${INPUT}" \
	--parameters clickhouse=true \
	--parameters clickhouse-host=${CLICKHOUSE_HOST} \
	--parameters clickhouse-user=${CLICKHOUSE_USER} \
	--parameters clickhouse-password=${CLICKHOUSE_PASSWORD} \
	--region "${REGION}"
