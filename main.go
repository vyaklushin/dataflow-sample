package main

import (
	"context"
	"flag"
	"fmt"

	"github.com/apache/beam/sdks/v2/go/pkg/beam"
	"github.com/apache/beam/sdks/v2/go/pkg/beam/io/bigqueryio"
	"github.com/apache/beam/sdks/v2/go/pkg/beam/io/pubsubio"
	"github.com/apache/beam/sdks/v2/go/pkg/beam/io/textio"
	"github.com/apache/beam/sdks/v2/go/pkg/beam/log"
	"github.com/apache/beam/sdks/v2/go/pkg/beam/options/gcpopts"
	"github.com/apache/beam/sdks/v2/go/pkg/beam/register"
	"github.com/apache/beam/sdks/v2/go/pkg/beam/x/beamx"

	t "gitlab.com/vyaklushin/dataflow-sample/internal/transformation"
)

var (
	// Path to log file
	input = flag.String("input", "", "Log file to read.")

	// Pub/Sub topic name
	topic = flag.String("topic", "", "Pub/Sub topic.")

	// Name of the BigQuery table
	table = flag.String("table", "project.dataset.example", "BigQuery table name")

	// Set this required option to specify where to write the output.
	output = flag.String("output", "", "Output file (only when debug mode on).")

	// Set debug mode to write into the file
	debugMode = flag.Bool("debug", false, "Debug mode (disabled by default).")

	// Set clickhouse mode to push into Clickhouse database
	clickhouseMode = flag.Bool("clickhouse", false, "Clickhouse mode (disabled by default).")

	clickhouseHost     = flag.String("clickhouse-host", "localhost:9000", "Clickhouse host")
	clickhouseUser     = flag.String("clickhouse-user", "default", "Clickhouse user")
	clickhousePassword = flag.String("clickhouse-password", "", "Clickhouse password")
)

// Register functions
func init() {
	register.DoFn3x0[context.Context, string, func(t.ApplicationLog)](&t.ParseJsonFn{})
	register.DoFn3x0[context.Context, t.ApplicationLog, func(t.BigQueryRecord)](&t.ProcessJsonFn{})
	register.DoFn3x0[context.Context, t.BigQueryRecord, func(t.BigQueryRecord)](&t.ClickhouseBatchFn{})
	register.Emitter1[t.BigQueryRecord]()
}

// Subscribes and reads from the PubSub topic
func ReadFromPubSub(s beam.Scope, project string, topic string) beam.PCollection {
	s = s.Scope("ReadPubSub")

	col := pubsubio.Read(s, project, topic, nil)

	return beam.ParDo(s, func(b []byte, emit func(string)) {
		emit(string(b))
	}, col)
}

// Convert json strings into JSON structures
func ParseJson(s beam.Scope, lines beam.PCollection) beam.PCollection {
	s = s.Scope("ParseJSON")

	return beam.ParDo(s, &t.ParseJsonFn{}, lines)
}

// Extract logs that match requirements
func ProcessJson(s beam.Scope, lines beam.PCollection) beam.PCollection {
	s = s.Scope("ProcessJSON")

	return beam.ParDo(s, &t.ProcessJsonFn{}, lines)
}

// Insert logs with transfer data in BigQuery table
func WriteInBigQuery(s beam.Scope, project string, lines beam.PCollection) {
	s = s.Scope("WriteInBigQuery")

	bigqueryio.Write(s, project, *table, lines)
}

func WriteInClickHouse(s beam.Scope, lines beam.PCollection) {
	s = s.Scope("WriteInClickhouse")

	beam.ParDo(s, &t.ClickhouseBatchFn{
		Host:     *clickhouseHost,
		User:     *clickhouseUser,
		Password: *clickhousePassword,
	}, lines)
}

// Create a file with logs that match criteria (Debug mode)
func LogInFile(ctx context.Context, s beam.Scope, lines beam.PCollection) {
	s = s.Scope("LogInFile")

	log.Infof(ctx, "Writing into file: %s", *output)

	debug := beam.ParDo(s, func(record t.BigQueryRecord, emit func(string)) {
		emit(fmt.Sprintf("%#v", record))
	}, lines)

	textio.Write(s, *output, debug)
}

// Reads logs from PubSub or File
func readLogs(ctx context.Context, s beam.Scope) beam.PCollection {
	var logLines beam.PCollection

	if *topic != "" {
		log.Infof(ctx, "Reading from Pub/Sub topic: %s", *topic)
		logLines = ReadFromPubSub(s, gcpopts.GetProject(ctx), *topic)
	} else {
		log.Infof(ctx, "Reading from file: %s", *input)
		logLines = textio.Read(s, *input, textio.ReadAutoCompression())
	}

	return logLines
}

// Saves metrics into LogFile / ClickHouse / BigQuery
func saveDataTransferMetrics(ctx context.Context, s beam.Scope, processedRecords beam.PCollection) {
	if *debugMode {
		LogInFile(ctx, s, processedRecords)
	} else if *clickhouseMode {
		WriteInClickHouse(s, processedRecords)
	} else {
		WriteInBigQuery(s, gcpopts.GetProject(ctx), processedRecords)
	}
}

func main() {
	flag.Parse()
	// beam.Init() is an initialization hook that must be called on startup.
	beam.Init()

	ctx := context.Background()

	log.Info(ctx, "Starting pipeline.")

	if *debugMode && *output == "" {
		log.Exitln(ctx, "No output provided")
	}

	if !*debugMode && *table == "" {
		log.Exitln(ctx, "No table provided")
	}

	p, s := beam.NewPipelineWithRoot()

	// Read logs from PubSub or File
	logs := readLogs(ctx, s)
	// Extract JSON objects from logs
	parsedRecords := ParseJson(s, logs)
	// Extract object that include Data Transfer metrics
	processedRecords := ProcessJson(s, parsedRecords)
	// Save metrics
	saveDataTransferMetrics(ctx, s, processedRecords)

	if err := beamx.Run(ctx, p); err != nil {
		log.Fatalf(ctx, "Failed to execute job: %v", err)
	}
}
