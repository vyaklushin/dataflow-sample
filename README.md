### Readme

This application is a Dataflow pipeline. 

It implements following functionality:
* Read application logs from GCS bucket
* Extract fields from application logs
* Insert extracted fields into BigQuery database

### Requirements

* Google Cloud Sandbox
* `gcloud` - https://cloud.google.com/sdk/docs/install-sdk (or with `brew install --cask google-cloud-sdk`)
* `go` 1.19+

### Initialize gcloud

1. Create a Google Cloud Sandbox account (https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project)
2. `gcloud init`
3. `gcloud auth login`

### How to use

Create a copy of .env.example

``` sh
cp .env.example .env
```

Update ENV variables that missing values.


To write parsing results into the file:

```sh
INPUT=examples/workhorse-sample.json OUTPUT=test.json make debug
 ```

 To write parsing results into BigQuery:

```sh
INPUT=log.json make process
 ```

To compile linux version of the app:

```sh
make build
```

To upload new version of the app to Google Cloud:

```sh
make gcloud_build
```

To run Dataflow pipeline:

```sh
INPUT=log.json make dataflow_run
```
