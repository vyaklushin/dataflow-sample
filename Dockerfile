FROM gcr.io/dataflow-templates-base/go-template-launcher-base

ARG WORKDIR=/dataflow/template
RUN mkdir -p ${WORKDIR}

COPY log_parser ${WORKDIR}/log_parser

ENV FLEX_TEMPLATE_GO_BINARY="${WORKDIR}/log_parser"

ENTRYPOINT ["/opt/google/dataflow/go_template_launcher"]
