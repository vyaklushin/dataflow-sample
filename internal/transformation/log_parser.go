package transformation

import (
	"context"
	"encoding/json"

	"github.com/apache/beam/sdks/v2/go/pkg/beam/log"
)

type ApplicationLog struct {
	Time string `json:"time"`
	// Workhorse fields
	WorkhorseRoute string `json:"route"`
	WorkhorseBytes int64  `json:"written_bytes"`
	WorkhorseUri   string `json:"uri"`
}

type ParseJsonFn struct{}

// ProcessElement for ParseJsonFn parses each log line, extracts fields from it and returns
// ApplicationLog elements
func (f *ParseJsonFn) ProcessElement(ctx context.Context, line string, emit func(ApplicationLog)) {
	var applicationLog ApplicationLog

	err := json.Unmarshal([]byte(line), &applicationLog)

	if err != nil {
		log.Errorln(ctx, "Error while decoding the data", err.Error())
	}

	if applicationLog != (ApplicationLog{}) {
		emit(applicationLog)
	}
}
