package transformation

import (
	"context"
	"crypto/tls"

	clickhouse "github.com/ClickHouse/clickhouse-go/v2"
	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"

	"github.com/apache/beam/sdks/v2/go/pkg/beam/log"
)

// Clickhouse configuration
type ClickhouseBatchFn struct {
	Host     string
	User     string
	Password string
	conn     clickhouse.Conn
	batch    driver.Batch
}

func (f *ClickhouseBatchFn) Setup() error {
	ctx := context.Background()

	conn, err := clickhouse.Open(&clickhouse.Options{
		Addr: []string{f.Host},
		TLS: &tls.Config{
			InsecureSkipVerify: true,
		},
		Auth: clickhouse.Auth{
			Database: "default",
			Username: f.User,
			Password: f.Password,
		},
	})

	if err != nil {
		log.Fatalln(ctx, "Cannot connect to clickhouse")
		return err
	}

	if err = conn.Ping(ctx); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			log.Fatalf(ctx, "Exception [%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		}
		return err
	}

	f.conn = conn
	return nil
}

func (f *ClickhouseBatchFn) StartBundle(ctx context.Context, _ func(BigQueryRecord)) error {
	batch, err := f.conn.PrepareBatch(ctx, "INSERT INTO transfer_data")
	if err != nil {
		return err
	}

	f.batch = batch

	return nil
}

func (f *ClickhouseBatchFn) ProcessElement(ctx context.Context, record BigQueryRecord, emit func(BigQueryRecord)) {
	err := f.batch.Append(
		record.CreatedAt,
		uint64(record.Bytes),
		record.Project,
		record.Namespace,
		record.Type,
	)
	if err != nil {
		log.Fatalln(ctx, err)
	}

	emit(record)
}

func (f *ClickhouseBatchFn) FinishBundle(ctx context.Context, _ func(BigQueryRecord)) error {
	err := f.batch.Send()
	if err != nil {
		log.Fatalln(ctx, err)
	}

	return nil
}
