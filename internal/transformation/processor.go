package transformation

import (
	"context"
	"strings"
	"time"

	"github.com/apache/beam/sdks/v2/go/pkg/beam/log"
)

const (
	typeGit = "git"
)

type BigQueryRecord struct {
	Bytes     int64     `bigquery:"bytes"`
	Project   string    `bigquery:"project"`
	Namespace string    `bigquery:"namespace"`
	Type      string    `bigquery:"type"`
	CreatedAt time.Time `bigquery:"created_at"`
}

type ProcessJsonFn struct{}

// ProcessElement for ParseJsonFn parses each log line, extracts fields from it and returns
// BigQueryRecord elements
func (f *ProcessJsonFn) ProcessElement(ctx context.Context, applicationLog ApplicationLog, emit func(BigQueryRecord)) {
	if applicationLog.WorkhorseRoute == "^/.+\\.git/git-upload-pack\\z" {

		tmp := strings.Split(applicationLog.WorkhorseUri, ".git/git-upload-pack")[0]
		namespace := strings.Split(tmp, "/")[1]
		project := strings.Join(strings.Split(tmp, "/")[1:], "/")

		record := BigQueryRecord{
			Type:      typeGit,
			CreatedAt: parseTime(ctx, applicationLog.Time),
			Bytes:     applicationLog.WorkhorseBytes,
			Project:   project,
			Namespace: namespace,
		}

		log.Infof(ctx, "Workhorse match: %v", record)
		emit(record)
	}
}

func parseTime(ctx context.Context, t string) time.Time {
	layout := "2006-01-02T15:04:05.000Z"
	timestamp, err := time.Parse(layout, t)

	if err != nil {
		layout := "2006-01-02T15:04:05Z"
		timestamp, err = time.Parse(layout, t)

		if err != nil {
			log.Fatalf(ctx, "Failed to parse timestamp: %v", err)
		}
	}

	return timestamp
}
